// import logo from './logo.svg';
import './App.css';
import Homepage from './pages/homepage/index';

function App() {
  return (
    <div className="App">
      <Homepage title='Easy Code'/>
    </div>
  );
}

export default App;
