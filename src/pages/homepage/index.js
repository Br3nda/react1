import React from 'react';
import Header from '../../components/Header';
import List from '../../components/List';

const homepage = (props) => {
  const myTitle = <h1 className='test1'>To jest kurs react.js {props.title}</h1>;

  const myTitle2 = React.createElement('h1', { className: 'test2' }, 'To jest kurs react.js nr 2');

  const listEle=['pomidorowa', 'ziemniaczana']

  return (
    <div className="App">
      <header className="App-header">
        {myTitle}
        {myTitle2}
        <Header name='Piotr'/>
        <Header name='Asia'/>
        <Header name='Paweł'/>
        {/* <Header name='Marta'/> */}
        <p>
          To jest mój pierwszy komponent
        </p>
      </header>
      <List listEl={listEle}/>
    </div>
  );
}

export default homepage;