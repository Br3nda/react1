// import React, { Component } from 'react';

const List = (props) => {
   const mapLi = props.listEl.map(li => {
      return <li key={li}>{li}</li>
   })
   return ( 
      <ul>
         {mapLi}
      </ul>
    );
}

// class List extends Component{
//    render() {
//       this.mapLi = this.props.listEl.map(li => {
//          return <li key={li}>{li}</li>
//       })
//       return (
//          <ul>
//             {this.mapLi}
//          </ul>
//       )
//    }
// }
 
export default List;